package model;


public class ContaPoupanca extends Conta {
	private Double taxaJuros;

	public Double getTaxaJuros() {
		return taxaJuros;
	}

	public void setTaxaJuros(Double taxaJuros) {
		this.taxaJuros = taxaJuros;
	}
	
	public void contabilizaJuros(){
		this.setSaldo(this.getSaldo()+(this.getSaldo()*(this.getTaxaJuros()/100)));
	}
}
