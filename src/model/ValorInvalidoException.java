package model;

public class ValorInvalidoException extends RuntimeException {
	//Exercícios 5 e 6;
	public ValorInvalidoException(Double valor) {
		super("Valor inválido: "+valor);
	}
	
	
}
