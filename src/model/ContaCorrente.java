package model;


public class ContaCorrente extends Conta{
	private Double valorManutencao;

	public Double getValorManutencao() {
		return valorManutencao;
	}

	public void setValorManutencao(Double valorManutencao) {
		this.valorManutencao = valorManutencao;
	}
	
	public void cobraManutencao(){
		this.setSaldo(this.getSaldo()-this.getValorManutencao());
	}
}
