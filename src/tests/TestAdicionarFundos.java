package tests;

import static org.junit.Assert.*;

import model.Conta;
import model.ContaPoupanca;
import model.ValorInvalidoException;

import org.junit.Before;
import org.junit.Test;

public class TestAdicionarFundos {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAdicionarFundosValorInvalido() {
		Conta cp =  new ContaPoupanca();
		try{
		cp.adicionarFundos(-100.0);
		}catch(ValorInvalidoException e){
			System.out.println(e.getMessage());//Exercicio 4
		}
	}
}
